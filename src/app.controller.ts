import { Controller, Get, Param } from '@nestjs/common';
import { AppService } from './app.service';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Get()
  getHello(): string {
    return this.appService.getHello();
  }

  @Get('getPassword/:password/:match')
  findOne(@Param('password') password: string, @Param('match') match: string) {
    return this.appService.getPassword(password, match);
  }

  @Get('createPassword/:password')
  createPassword(@Param('password') password: string) {
    return this.appService.createPassword(password);
  }
}
