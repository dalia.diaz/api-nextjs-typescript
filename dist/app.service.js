"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AppService = void 0;
const common_1 = require("@nestjs/common");
const bcrypt_1 = require("bcrypt");
let AppService = class AppService {
    getHello() {
        return "Hello World!";
    }
    createItem() {
        return "Hello Items!";
    }
    async getPassword(password, match) {
        const passwordMatch = await (0, bcrypt_1.compare)(password, match);
        const res = JSON.stringify({ status: true, password: passwordMatch });
        return { status: true, message: "Success", password: passwordMatch };
    }
    async createPassword(password) {
        const passwordHashed = await (0, bcrypt_1.hash)(password, 10);
        const res = JSON.stringify({
            status: true,
            message: "Success",
            password: passwordHashed,
        });
        return {
            status: true,
            message: "Success",
            password: passwordHashed,
        };
    }
};
AppService = __decorate([
    (0, common_1.Injectable)()
], AppService);
exports.AppService = AppService;
//# sourceMappingURL=app.service.js.map