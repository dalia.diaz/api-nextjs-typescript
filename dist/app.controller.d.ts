import { AppService } from './app.service';
export declare class AppController {
    private readonly appService;
    constructor(appService: AppService);
    getHello(): string;
    findOne(password: string, match: string): Promise<any>;
    createPassword(password: string): Promise<any>;
}
