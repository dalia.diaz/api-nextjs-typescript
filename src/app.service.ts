import { Injectable } from "@nestjs/common";
import { compare, hash } from "bcrypt";

@Injectable()
export class AppService {
  getHello(): string {
    return "Hello World!";
  }
  createItem(): any {
    return "Hello Items!";
  }
  async getPassword(password, match): Promise<any> {
    const passwordMatch = await compare(password, match);
    const res = JSON.stringify({ status: true, password: passwordMatch });
    return { status: true, message: "Success", password: passwordMatch };
  }

  async createPassword(password): Promise<any> {
    const passwordHashed = await hash(password, 10);
    const res = JSON.stringify({
      status: true,
      message: "Success",
      password: passwordHashed,
    });

    return {
      status: true,
      message: "Success",
      password: passwordHashed,
    };
  }
}
