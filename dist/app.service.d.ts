export declare class AppService {
    getHello(): string;
    createItem(): any;
    getPassword(password: any, match: any): Promise<any>;
    createPassword(password: any): Promise<any>;
}
